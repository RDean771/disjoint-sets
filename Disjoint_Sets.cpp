﻿#include <iostream>

#include <unordered_map>

#include <utility>


using namespace std;



class Node {
	
  public:
	  
    int data;
    Node* parent;
    int rank;

    Node (){}
    Node (int);

    ~Node (){}

};



Node:: Node (int data) {

  this->data = data;
	
  this->parent = this;
	
  this->rank = 0;

}



class DisjointSet {

  public:

    unordered_map <int, Node*> dict;

    void makeSet (int);

    Node* findSet (int);

    DisjointSet () {}

    void unija (int, int);

    ~DisjointSet () {}

};



void DisjointSet:: makeSet (int data) {
	
  Node* node = new Node (data);
	
  this->dict.insert (make_pair (data, node));

}



Node* DisjointSet:: findSet(int data) {
	 
  unordered_map<int, Node*> :: const_iterator got = this->dict.find (data);
	 
  Node* node = got -> second;
	 
  if (node->parent != node)
	   
    node -> parent = findSet(node->parent->data);
	 
  return node->parent;

}



void DisjointSet:: unija (int data1, int data2) {
	
  Node* node1 = findSet(data1);
	
  Node* node2 = findSet(data2);
	
  if (node1 == node2) return;
	
  if (node1 -> rank > node2 -> rank)
	  
    node2 -> parent = node1;
	
  else {

    node1 -> parent = node2;

    if(node1 -> rank == node2 -> rank)
		  
      (node2 -> rank)++;
	
  }

}


int main () {
	
  DisjointSet DS;
	
  for (int i = 1; i <= 7; i++)
	  
    DS.makeSet (i);

	
  for (int i = 1; i <= 7; i++)
	  
    cout << "Reprezentant skupa u kojem je broj " << i << " je: " << DS.findSet (i) -> data << endl;
	
  cout << endl;

    
  DS.unija (1, 2);

    
  for (int i = 1; i <= 7; i++)
	  
    cout << "Reprezentant skupa u kojem je broj " << i << " je: " << DS.findSet (i) -> data << endl;

  cout << endl;

    
  DS.unija (2, 3);

    
  for (int i = 1; i <= 7; i++)
	  
    cout << "Reprezentant skupa u kojem je broj " << i << " je: " << DS.findSet (i) -> data << endl;
	
  cout << endl;

    
  DS.unija (4, 5);

    
  for (int i = 1; i <= 7; i++)
	  
    cout << "Reprezentant skupa u kojem je broj " << i << " je: " << DS.findSet (i) -> data << endl;

  cout << endl;

    
  DS.unija (6, 7);

    
  for (int i = 1; i <= 7; i++)
	  
    cout << "Reprezentant skupa u kojem je broj " << i << " je: " << DS.findSet (i) -> data << endl;

  cout << endl;
    
  
  DS.unija (3, 5);

    
  for (int i = 1; i <= 7; i++)
	  
    cout << "Reprezentant skupa u kojem je broj " << i << " je: " << DS.findSet (i) -> data << endl;

  cout << endl;

    
  DS.unija (1, 7);

    
  for (int i = 1; i <= 7; i++)
	  
  cout << "Reprezentant skupa u kojem je broj " << i << " je: " << DS.findSet (i) -> data << endl;

  
  return 0;

}